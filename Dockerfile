FROM debian:jessie
MAINTAINER O.O.B <omer.orhan@metglobal.com>

# Install some deps, lessc and less-plugin-clean-css, and wkhtmltopdf
RUN set -x; \
        apt-get update \
        && apt-get install -y --no-install-recommends \
            ca-certificates \
            curl \
            nodejs \
            npm \
	    vim \
            python-support \
            python-pyinotify \
        && npm install -g less less-plugin-clean-css \
        && ln -s /usr/bin/nodejs /usr/bin/node \
        && curl -o wkhtmltox.deb -SL http://nightly.odoo.com/extra/wkhtmltox-0.12.1.2_linux-jessie-amd64.deb \
        && echo '40e8b906de658a2221b15e4e8cd82565a47d7ee8 wkhtmltox.deb' | sha1sum -c - \
        && dpkg --force-depends -i wkhtmltox.deb \
        && apt-get -y install -f --no-install-recommends \
        && apt-get -y install -f python-pip \
        && apt-get -y install -f python-dev \
        && apt-get -y install -f libpq-dev \
        && apt-get -y install -f libxml2-dev \
        && apt-get -y install -f libxslt1-dev \
        && apt-get -y install -f libldap2-dev \
        && apt-get -y install -f libsasl2-dev \
        && apt-get -y install -f build-essential \
        && apt-get -y install -f git \
        && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false -o APT::AutoRemove::SuggestsImportant=false npm \
        && rm -rf /var/lib/apt/lists/* wkhtmltox.deb

# Basic stuff

# Install odoo
# SSH step
RUN mkdir /root/.ssh 
ADD deploykey /root/.ssh/id_rsa
RUN chmod 0600 /root/.ssh/id_rsa
RUN touch /root/.ssh/known_hosts \ 
    && apt-get update \
    && apt-get -y install openssh-client \
    && apt-get -y install rsync debhelper fakeroot
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

# Build and install odoo
RUN git clone -b 8.0 git@bitbucket.org:metglobal/odoo.git /opt/odoo \
    && apt-get -y install -f postgresql-client \
    && apt-get -y install -f python-dateutil \
    && apt-get -y install -f python-decorator \
    && apt-get -y install -f python-docutils \
    && apt-get -y install -f python-feedparser \
    && apt-get -y install -f python-imaging \
    && apt-get -y install -f python-jinja2 \
    && apt-get -y install -f python-ldap \
    && apt-get -y install -f python-lxml \
    && apt-get -y install -f python-mako \
    && cd /opt/odoo && dpkg-buildpackage -us -uc \
    && dpkg --force-depends -i ../*.deb \
    && cp -af /opt/odoo/addons/* /usr/lib/python2.7/dist-packages/openerp/addons \
    && rm -rf /opt/odoo*

# Install paytrek dependencies
RUN git clone -b 1.1.4 git@bitbucket.org:metglobal/odoo-paytrek-modules.git /opt/paytrek \
    && pip install -r /opt/paytrek/requirements.txt

COPY ./entrypoint.sh /
COPY ./openerp-server-paytrek.conf /etc/odoo/
# Fix perms
#
RUN  chown odoo /etc/odoo/openerp-server-paytrek.conf \
     && touch /var/log/odoo-server.log \
     && chown odoo /var/log/odoo-server.log

# Mount /var/lib/odoo to allow restoring filestore and /mnt/extra-addons for users addons
VOLUME ["/var/lib/odoo", "/var/log", "/opt/paytrek”, "/opt/odoo"]

# Expose Odoo services
# EXPOSE 896 872

# Set the default config file
ENV OPENERP_SERVER /etc/odoo/openerp-server-paytrek.conf

# Set default user when running the container
USER odoo

ENTRYPOINT ["/entrypoint.sh"]
CMD ["openerp-server"]